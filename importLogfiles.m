function [Files, Nlog] = importLogfiles
%importLogfiles function to identify the location of Log files created during the CT reconstruction in
%different folders
%   The files aren't loaded into the memory directly.

%Ask user to select bottom scan
hb = helpdlg('Select log-file with reconstruction of bottom half','Select bottom');
uiwait(hb);
[file_bottom,path_bottom] = uigetfile({'*.log'},pwd,'Select log-file with CT reconstruction bottom half');
% if no files are selected give feedback to command window
if isequal(file_bottom,0)
   disp('User selected Cancel')
else
    file_bottom = fullfile(path_bottom,file_bottom);
end
    
%Ask user to select top scan
ht = helpdlg('Select log-file with reconstruction of top half','Select top');
uiwait(ht);
[file_top,path_top] = uigetfile({'*.log'},'Select log-file with CT reconstruction top half');
% if no files are selected give feedback to command window
if isequal(file_top,0)
   disp('User selected Cancel')
   else
    file_top = fullfile(path_top,file_top);
end

Files={file_bottom,file_top};
%Number of found log-files
Nlog = numel(Files);
end